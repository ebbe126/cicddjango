from django.db import models

# Create your models here.
from django.db import models


class Category(models.Model):
    #  name = models.CharField(max_length=30)
    ar_name = models.CharField(max_length=30)
    fr_name = models.CharField(max_length=30)
    image = models.CharField(max_length=30)
    prev = models.CharField(max_length=30)
    next = models.CharField(max_length=30)


class Product(models.Model):
    category = models.ForeignKey(Category, related_name='products',
                                 on_delete=models.CASCADE)
    name = models.CharField(max_length=200, db_index=True)
    slug = models.SlugField(max_length=200, db_index=True)
    image = models.ImageField(upload_to='products/%Y/%m/%d', blank=True)
    description = models.TextField(blank=True)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    available = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
