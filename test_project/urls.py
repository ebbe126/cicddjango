from django.contrib import admin
from django.urls import path
from rest_framework import routers
from user.viewsets import UserViewSet

urlpatterns = [
    path('/', admin.site.urls)

]

